'''
This code reproduces error when both spacy and sentence_splitter are active
https://github.com/berkmancenter/mediacloud-sentence-splitter/issues/2
'''

import spacy
from sentence_splitter import SentenceSplitter

nlp = spacy.load('en_core_web_sm')
doc = nlp("This is a sample sentence.")

splitter = SentenceSplitter(language='en')
splitter.split("This is a sample sentence.")

# nlp = spacy.load('it_core_news_sm')
# doc = nlp("Ecco, io sono qua oggi per parlarvi di esoscheletri, cioè robot indossabili.")

# splitter = SentenceSplitter(language='it')
# splitter.split("Ecco, io sono qua oggi per parlarvi di esoscheletri, cioè robot indossabili.")

