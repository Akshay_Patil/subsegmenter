import os 

current_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.join(current_path, os.pardir)
subtitle_path = os.path.join(parent_path, 'subtitles')

test_subtitle_flat = os.path.join(subtitle_path, 'gallimberti.txt')
test_subtitle_segmented_auto = os.path.join(subtitle_path, 'gallimberti_auto.txt')
test_subtitle_segmented = os.path.join(subtitle_path, 'gallimberti_segmented.txt')

cescati_subtitle_flat = os.path.join(subtitle_path, 'cescati.txt')
cescati_subtitle_segmented_auto = os.path.join(subtitle_path, 'cescati_auto.txt')
