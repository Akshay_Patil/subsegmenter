BUTTON_INFO = 'ℹ️ INFO'

INFO_QUESTIONS_LOWER = ['chi sei?', 'cosa fai?', 'come funzioni?']

def intro():
    return (
        "😎 Ciao , sono un segmentatore di sottotitoli ✂️.\n"
        "Puoi fare una delle seguenti cose:\n"
        "📁 inviarmi un *file txt* da segmentare (puro testo)\n" 
        "🗜 inviarmi un *file zip* con più file di testo da segmentare\n"
        "✍️ scrivere un *testo* da segmentare (non più di 2000 caratteri)"
    )

def wrong_input(text):
    reply = "Comando non riconosciuto: {}"
    return reply.format(text)            
        
