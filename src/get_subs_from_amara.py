import requests
from key import AMARA_HEADERS
import json

# get the subtitles data
def get_subtitles(url, headers, parameters, pagination_data):

    # get the video ids checking the language and the published status
    selected_video_info = [] # tuples (original_lang, video_id)
    for video_info in pagination_data['objects']:
        for lang_info in video_info['languages']:
            if (lang_info['code']) == 'it' and (lang_info['published']) == True:
                selected_video_info.append([video_info['original_language'], video_info['id']])

    # get the subtitles data and add the "original_language" key to the subtitles dictionary
    for original_lang,video_id in selected_video_info:
        sub_url = url + video_id + '/languages/it/subtitles/'
        #print(sub_url)
        response = requests.get(sub_url, headers=headers, params=parameters)
        sub_json = response.json()
        
        output_file = '../subtitles/amara/json/{}_{}.json'.format(original_lang, video_id)
        with open(output_file, 'w') as f_out:
            json.dump(sub_json, f_out, indent=3)
        print(output_file)


def run_script():
    i = 0
    page_size = 100
    initial_offset = 0 # 57900 # to re-start if interrupted
    while(True):

        offset = initial_offset + page_size * i
        url = 'https://amara.org/api/videos/'
        parameters = {
            'team': 'ted',
            'project': 'tedxtalks',
            'offset': offset,
            'limit': page_size
        }        

        response = requests.get(url, headers=AMARA_HEADERS, params=parameters)
        pagination_data = response.json()
        meta_data = pagination_data['meta']
        total_count = meta_data['total_count']
        print('{}-{}/{}'.format(offset+1, offset+page_size, total_count))

        get_subtitles(url, AMARA_HEADERS, parameters, pagination_data)

        if meta_data['next'] is None:
            break

        i += 1


if __name__ == "__main__":
    run_script()
