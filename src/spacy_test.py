import spacy
from spacy import displacy

nlp = spacy.load('it_core_news_sm')
#nlp = spacy.load('en_core_web_sm')
#doc = nlp(u"Autonomous cars shift insurance liability toward manufacturers")
#doc = nlp("Nel mondo ci sono 22 milioni di consumatori di cocaina. Di questi, 9 milioni sono distribuiti nelle Americhe e 5 milioni in Europa. Perché la cocaina, che sembra continui ad espandersi senza sosta, ha questo grande appeal nei confronti degli abitanti del nostro pianeta? La risposta è abbastanza semplice. Perché produce un grande piacere. Un piacere che è tremendamente simile a quello del primo amore e quindi un piacere che tutti ricordiamo e che tutti vorremmo riprovare come nel momento in cui lo abbiamo provato la prima volta. Ma se fosse per questo motivo, la cocaina sarebbe sì un problema ma relativamente.")
#doc = nlp("Nel mondo ci sono 22 milioni di consumatori di cocaina.")
#doc = nlp("Nel mondo , ci sono 22 milioni di consumatori di cocaina e di droghe pesanti.")
#doc = nlp("Un piacere che è tremendamente simile a quello del primo amore, e quindi un piacere che tutti ricordiamo e che tutti vorremmo riprovare [come] nel momento in cui lo abbiamo provato la prima volta.")
doc = nlp("Ecco, io sono qua oggi per parlarvi di esoscheletri, cioè robot indossabili.")


for token in doc:
    #print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_, token.shape_, token.is_alpha, token.is_stop)
    print("{} -> {}".format(token.text, ' '.join(t.text for t in token.children)))

tokens = list(doc)
first_tok = tokens[0]
second_tok = tokens[1]
first_tok_children = list(first_tok.children) # [mondo]
first_tok_ancestors = list(first_tok.ancestors) # [sono]
second_tok_ancestors = list(second_tok.ancestors) # [Nel, sono]


# tok.is_ancestor: Check whether this token is a parent, grandparent, etc. of another in the dependency tree.
# tok.ancestors:
# tok.children: A sequence of the token's immediate syntactic children.
# tok.lefts: The leftward immediate children of the word, in the syntactic dependency parse. 
# tok.rights: The rightward immediate children of the word, in the syntactic dependency parse. 
# tok.subtree

# tok.conjuncts: A sequence of coordinated tokens, including the token itself.

displacy.serve(doc, style='dep')

'''
universaldependencies tagset
ADJ: adjective
ADP: adposition
ADV: adverb
AUX: auxiliary
CCONJ: coordinating conjunction
DET: determiner
INTJ: interjection
NOUN: noun
NUM: numeral
PART: particle
PRON: pronoun
PROPN: proper noun
PUNCT: punctuation
SCONJ: subordinating conjunction
SYM: symbol
VERB: verb
X: other
'''